__author__ = 'Xurxo'

import csv

csv.field_size_limit(1000000000)
users = []
apps = []
userApp = []
freq = []
jj = 0

with open('data/appsUsage.csv', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter='\t')
    for row in reader:
        if len(row) > 0:
            if (row[1] != "TND") and (row[1] != "notInFile"):
                ii = 0
                lock = False
                for aux in userApp:
                    if [row[0], row[1]] == aux[:2]:
                        lock = True
                        ii = userApp.index(aux)
                if not lock:
                    line = [row[0], row[1], 1]
                    userApp.append(line)
                    jj += 1
                else:
                    userApp[ii][2] += 1

with open('data/appsIndividualUsage.csv', 'w', newline='') as csvfilewrite:
    writer = csv.writer(csvfilewrite, delimiter=";", quotechar=';')
    for r in userApp:
        writer.writerow(r)